<?php

class Environment {

    /*
     * return boolean
     */

    protected function check_file_exist($file) {
        return file_exists($file);
    }

    /*
     * return boolean
     */

    protected function check_dir_access($dir) {
        return is_writable($dir);
    }

    /*
     * return string
     */

    protected function press_key(){
        return intval(ord((fgets(STDIN))));
    }

    /*
     * return string
     */

    protected function line () {
        echo '---------------------------------------------------------------'.PHP_EOL;
    }

    protected function get_file($file) {
        return file_get_contents($file);
    }


}

class Start extends Environment {

    public $key;

    public $file;

    public function __construct()
    {
        if (!($this->check_dir_access('/etc/apache2/'))) {
            echo 'Using this program with root permission'.PHP_EOL;
            die();
        }

        $this->header_info();

        $this->start();

    }

    protected function header_info() {
        $this->line();

        echo 'Welcom to program create virtual host'.PHP_EOL;

        $this->line();

    }

    protected function start () {
        echo 'If you want to create a host input key \'1\' else del key \'0\','.PHP_EOL;
        echo 'To view the log file input key \'3\''.PHP_EOL;
        $this->line();
        echo 'Enter key -> ';

        $key = $this->press_key();
        $this->line();

        if($key == 49) {
            $this->key = $key;
        } elseif ($key == 48) {
            $this->key = $key;
        } elseif ($key == 51) {
            $this->show_log_file();
        } else {
            echo 'Invalid input key'.PHP_EOL;
            die();
        }
    }

    protected function show_log_file() {

            $file = $this->get_file('log.json');

            echo $file.PHP_EOL;
            $this->line();
            echo 'To continue working input key \'1\''.PHP_EOL;
            echo 'To exit the program input key \'0\''.PHP_EOL;
            $this->line();
            echo 'Enter key -> ';
            $key = $this->press_key();
            $this->line();
            if($key == 49) {
                $this->start();
            } else {
                echo 'Have a nice time'.PHP_EOL;
                die();
            }

    }

}

class Host extends Environment {
    protected $template;

    protected $config;

    public function __construct()

    {
        $this->init();
    }


    protected function init () {

        $this->create_template();
        $this->get_json();
        $this->template = strtr($this->template, $this->config);
        $this->set_config_site($this->config['{{ServerName}}'], $this->template);
        $this->create_log_dir($this->config['{{ServerName}}']);
        $this->create_dir_host($this->config['{{DocumentRoot}}']);
        $this->create_string_host($this->config['{{ServerName}}']);
        $this->apache_restart();

    }

    protected function create_template() {
        if ($this->check_file_exist('template.php')) {
            $this->template = $this->get_file('template.php');
        } else {
            echo 'Can\'t find file \'template.php\''.PHP_EOL;
            die();
        }
    }

    protected  function get_json() {
        if($this->check_file_exist('vs.json')){
            $str = $this->get_file('vs.json');
            $config = json_decode($str, true);
            $required = ['ServerName', 'ServerAdmin', 'DocumentRoot'];
            foreach ($required as $value) {
                if(empty($config[$value])) {
                    echo 'param '.$value.' required.'.PHP_EOL;
                    die();
                }
            }
            $result = [];
            foreach ($config as $key => $value) {
                $result['{{'.$key.'}}'] = $value;
            }
            $this->config = $result;
        } else {
            echo 'Can\'t find file \'vs.json\'';
            echo '{"ServerName": "", "ServerAdmin": "",  "DocumentRoot": ""}'."\n";
            die();
        }
    }

    protected function set_config_site ($file, $template) {

        $file_conf = '/etc/apache2/sites-available/'.$file.'.conf';

        if($this->check_file_exist($file_conf)) {
            $this->line();
            echo 'file '.$this->config['{{ServerName}}'].' exist'.PHP_EOL ;
            echo 'If you want to continue enter key \'1\', you will overwrite the file'.PHP_EOL;
            echo 'Enter the \'0\' key to terminate the program.'.PHP_EOL;
            $this->line();
            echo 'Enter key ';

            $key = $this->press_key();
            if($key == 49) {
                file_put_contents($file_conf, $template);
                echo 'file '.$file.' is overwritten'.PHP_EOL;
                $this->line();
            } elseif ($key == 48) {
                echo 'Close program'.PHP_EOL;
                die();
            } else {
                echo 'Invalid input key'.PHP_EOL;
                die();
            }


        } else {
            file_put_contents($file_conf, $template);
            echo 'file '.$file.' create'.PHP_EOL;
            $this->line();
        }

        exec('a2ensite '.$file.'.conf', $output);

        if (isset($output[0])) {
            echo $output[0].PHP_EOL;
            $this->line();
        }

    }

    protected function create_dir($dir_path) {
        if ($this->check_file_exist($dir_path)) {
            echo 'dir '.$dir_path.' exist'.PHP_EOL;
            $this->line();
            return true;
        }
    }

    protected function create_log_dir($dir) {

        $dir_path = '/var/log/apache2/'.$dir;

        if(!($this->create_dir($dir_path))) {
                exec('mkdir  '.$dir_path, $output);
                if($this->check_file_exist($dir_path)) {
                    echo 'File '.$dir_path.' create'.PHP_EOL;
                    $this->line();
            }
        }

    }

    protected function create_dir_host ($dir) {

        $dir_path = $dir;

        if(!($this->create_dir($dir_path))) {
            mkdir($dir_path, 0755);
            file_put_contents($dir_path.'/index.php', '<?php phpinfo(); ?>');
            if($this->check_file_exist($dir_path)) {
                echo 'File '.$dir_path.' create'.PHP_EOL;
                $this->line();
            }
        }
    }

    protected function create_string_host ($name) {

        $file_path = '/etc/hosts';

        if($this->check_file_exist($file_path)) {
            $text = '# Config file create '.date('d-m-Y').PHP_EOL;
            $text .= '127.0.0.1 '.$name.PHP_EOL;

            file_put_contents($file_path, $text, FILE_APPEND);

        } else {
            echo 'dir /etc/hosts not exist'.PHP_EOL;
            $this->line();
        }
    }

    protected function apache_restart() {
        exec('apache2ctl restart', $output);

        if (isset($output[0])) {
            echo $output[0].PHP_EOL;
        }
    }

}

class Del {

}

$start = new Start();

if ($start->key == 49) {
    $host = new Host();
} else if ($start->key == 48) {
    $host = new Del();
}